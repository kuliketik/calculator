import unittest
from calculator import Calculator

class CalculatorTestCase(unittest.TestCase):
    def setUp(self):
        self.c = Calculator()

    def tearDown(self):
        self.c = None

    def test_add(self):
        self.assertEqual(self.c.calculate('2 + 2'), 4, 'Add result not equal')

    def test_add_and_multiply(self):
        self.assertEqual(self.c.calculate('2 + 3 * 2'), 8, 'Add and Multipy not equal')

    def test_add_division_and_multiply(self):
        self.assertEqual(self.c.calculate('3*2/2+5'), 8, 'Add, Division, and Multiply not equal')

    def test_subtract_and_division(self):
        self.assertEqual(self.c.calculate('20 - 8 /4'), 18, 'Subtract and Division not equal')

if __name__ == '__main__':
        unittest.main()


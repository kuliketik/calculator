class Calculator:
    def __init__(self):
        self.op = ('+', '-', '*', '/')

    def parse(self, statement):
        self.operand = []
        self.operator = []
        token = ''
        for c in statement:
            if c.isdigit(): 
                token = token + c
            elif len([op for op in self.op if op == c]) > 0:
                self.operator.append(c)
                self.operand.append(int(token))
                token = ''

        self.operand.append(int(token))
        token = ''

    def operate(self):
        self.operand.reverse();
        output = 0
        result = [self.operand.pop(),]
        for op in self.operator:
            if op == '+':
                result.append(self.operand.pop())
            elif op == '-':
                result.append(-self.operand.pop())
            elif op == '*':
                tmp = result.pop() * self.operand.pop()
                result.append(tmp)
            elif op == '/':
                tmp = result.pop() / self.operand.pop()
                result.append(tmp)

        for res in result:
            output = output + res

        return output

    def calculate(self, statement):
        self.parse(statement)
        return self.operate()

